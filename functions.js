/**
 *
 * @param {number} a
 * @param {number} b
 *
 * @returns {number} the sum of a and b
 */
function sum(a, b) {
    return a + b;
}

/**
 *
 * @param {*} input
 */
function myFunction(input) {
    if (typeof input !== "number") {
        throw new Error("Invalid input");
    }
}

/**
 *
 * @param {*} callback
 */
function fetchData(callback) {
    setTimeout(() => {
        callback("data");
    }, 1000);
}

function fetchPromise(shouldReject = false) {
    return new Promise((resolve, reject) => {
        if (shouldReject) {
            reject("error");
        } else {
            setTimeout(() => {
                resolve("data");
            }, 1000);
        }
    });
}

export { sum, myFunction, fetchData, fetchPromise };
