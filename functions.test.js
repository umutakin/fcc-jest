// @ts-check
import { sum, myFunction, fetchData, fetchPromise } from "./functions";

test("adds 1 + 2 to equal 3", () => {
    expect(sum(1, 2)).toBe(3);
});

test("two plus two is four", () => {
    expect(2 + 2).toBe(4);
});

test("object assignment", () => {
    const data = { one: 1 };
    data["two"] = 2;
    expect(data).toEqual({ one: 1, two: 2 });
});

test("null is falsy", () => {
    const n = null; // 0, false, undefined, null
    expect(n).toBeFalsy();
});

test("one is truthy", () => {
    const one = 1;
    expect(one).toBeTruthy();
});

test("throws on invalid input", () => {
    expect(() => {
        myFunction("invalid input");
    }).toThrow();
});

// ASYNC

test("the data is data", (done) => {
    /** @param {string} data */
    function callback(data) {
        try {
            expect(data).toEqual("data");
            done();
            // return new Promise((resolve) => resolve(true));
        } catch (e) {
            done(e);
            // return new Promise((reject) => reject(false));
        }
    }

    fetchData(callback);
});

test("the data is data (promise)", () => {
    return expect(fetchPromise()).resolves.toBe("data");
});

test("the fetch test fails with an error", () => {
    return expect(fetchPromise(true)).rejects.toBe("error");
});

test("the data is data (async/await)", async () => {
    const data = await fetchPromise();
    expect(data).toBe("data");
});
